import requests

# Задача 1:
# Написать функцию, которая вычисляет длину вектора в трехмерном пространс
from math import *


def vector_len(x, y, z):
    result = sqrt(x*x + y*y + z*z)
    return result


a = vector_len(12,-3,5)
print(a)

# Задача 2:
# Написать функцию, которая вычисляет длину вектора в пятимерном пространстве



# Задача 3:
# Написать одну единственную функцию калькулятор для произвольного количества аргументов.
# Чтобы одна функция могла делать операции
# - сложение/вычитание
# - умножение/деление
# - сравнение первого и второго элемета на равенство (результат - boolean)
# - нахождение минимального/максимального числа
# - сумму/произведение всех элементов


def calculate(do, num, num2, *args):

    if do == '+':
        count1 = 0
        for x in args:
            count1 += x
        num_sum = num + num2 + count1
        return num_sum
    elif do == '-':
        count = 0
        for x in args:
            count += x
        num_diff = num - num2 - count
        return num_diff
    elif do == '*':
        count = 1
        for x in args:
            count *= x
        num_product = num * num2 * count
        return num_product
    elif do == '/':
        count = 1
        for x in args:
            count *= x
        if num and num2 and count != 0:
            num_ratio = num / num2 / count
            return num_ratio
        else:
            print('Нельзя делить на ноль')
    elif do == '==':
        if num == num2:
            return True
        if num != num2:
            return False
    elif do == 'min':
        num_list = []
        num_list.append(num)
        num_list.append(num2)
        num_list += list(args)
        return min(num_list)
    elif do == 'max':
        num_list = []
        num_list.append(num)
        num_list.append(num2)
        num_list += list(args)
        return max(num_list)
    elif do == 'sum':
        num_list = []
        num_list.append(num)
        num_list.append(num2)
        num_list += list(args)
        return sum(num_list)
    elif do == 'mult':
        num_list = []
        num_list.append(num)
        num_list.append(num2)
        num_list += list(args)
        res = 1
        for i in num_list:
            res *= i
        return res


s = calculate('/',17,2)
print(s)

# Задача 4:
# Написать функцию, которая примнимает в качестве параметра
#  - url страницы
#  - ключевое слово
#  - callback метод успех/неудача
#
# Определить методы успех/неудача.
#
# Искомая функция загружает страницу и если на странице есть ключевое слово, то запускает метод "успех",
# иначе "неудача".
# Протестировать на нескольких страницах.
import requests


def get_responce(url, keyword, success_function, fail_function):
    request_result = requests.get(url)
    text_version = request_result.text
    if str(text_version).find(keyword) >= 0:
        return success_function(keyword)
    else:
        return fail_function()


def success(word):
    return f'Словово "{word}" найдено на странице.'


def fail():
    return 'Такого слова на странице нет.'

def success2(word):
    return f'Так можно было? Вот "{word}" найдено на странице.'

a = get_responce('https://python.ru/post/97/', 'решения', success, fail)
print(a)

# Задача 5:
# Задать 20 случайных точек двумерном пространстве - x, y.
# 0 < x < 10, 0 < y < 10
# Написать функцию для нахождения точки между, которыми будет минимальное расстояние
import random

def point_len(point1, point2):
    x1 = point1['x']
    x2 = point2['x']
    y1 = point1['y']
    y2 = point2['y']
    len = sqrt((x2 - x1)**2 + (y2 -y1)**2)
    return len

tochka_dict = []
for tochki in range(4):
    x = random.randint(1,10)
    y = random.randint(1,10)
    d = {'x': x, 'y': y}
    tochka_dict.append(d)

a_list = []
for a in tochka_dict:
    for b in tochka_dict:
        if a != b:
            a_list.append(point_len(a, b))
        if b == a:
            break

print(min(a_list))




