import pygame as pg
import random

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (120, 158, 123, 255)
BLUE = (0, 0, 255)
GREY = (145, 145, 145, 255)
screen = pg.display.set_mode((300, 300))
screen.fill(WHITE)


def scale_set(color, x, y, list_name, frame_num):
    for qub_vert in range(30):
        for qub_hor in range(30):
            name = pg.draw.rect(screen, color, (x, y, 10, 10), frame_num)
            x += 10
            list_name.append(name)
        x -= 300
        y += 10


class PaintSet:
    color_choice = [WHITE, GREEN]

    qub_list = []
    scale_set(WHITE, 0, 0, qub_list, 0)
    my_color = (255, 255, 255)

    pqub_list = []
    for pqub in qub_list:
        pg.draw.rect(screen, random.choice(color_choice), (pqub[0], pqub[1], 10, 10))
        pqub_list.append(pqub)

    def __init__(self):
        pg.display.update()


pg.init()
running = True

while running:
    PaintSet()
    spisok = []
    # one_more_fucking_list = []
    # save_state = []
    for qub in PaintSet.pqub_list:
        feeler = pg.draw.rect(screen, WHITE, (qub[0] - 10, qub[1] - 10, 30, 30), 1)
        center_qub_color = screen.get_at((qub[0]+2, qub[1]+2))

        for qub1 in PaintSet.pqub_list:
            qubs_color = screen.get_at((qub1[0]+2, qub1[1]+2))
            if feeler.colliderect(qub1) and qubs_color == GREEN:
                spisok.append(qub1)

        if len(spisok) == 4:
            pg.draw.rect(screen, GREEN, (qub[0], qub[1], 10, 10))
        elif len(spisok) < 3:
            pg.draw.rect(screen, WHITE, (qub[0], qub[1], 10, 10))
        elif len(spisok) > 4:
            pg.draw.rect(screen, WHITE, (qub[0], qub[1], 10, 10))
        elif len(spisok) == 3 and center_qub_color == GREEN:
            pg.draw.rect(screen, GREEN, (qub[0], qub[1], 10, 10))

        spisok.clear()
    pg.display.update()
    for event in pg.event.get():
        if event.type == pg.QUIT:
            running = False
        if event.type == pg.MOUSEBUTTONUP:
            if event.button == 3:
                running = False




