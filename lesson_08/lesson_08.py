import random
import pygame
import copy
import pickle
from pygame_widgets import Button

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (252, 88, 97)
BLUE = (0, 0, 255)
GREY = (145, 145, 145, 255)
GREEN = (120, 158, 123, 255)
NUM_V_ELEMENTS = 40
NUM_H_ELEMENTS = 40
QUB_HIGH = 10
QUB_LEN = 10

CHANGE_SLIDE_SPEED = 20

CELL_BIRTH_POSSIBILITY = 0.07

GAME_STATE_PAUSE = 1
GAME_STATE_PLAY = 2

CELL_STATE_DEAD = 0
CELL_STATE_ALIVE = 1
# Тороидальная вселенная. Поле замыкается через края
TORR_UNIVERSE = False


pygame.init()

screen_size = (QUB_LEN * NUM_H_ELEMENTS, QUB_HIGH * NUM_V_ELEMENTS + 70)
screen = pygame.display.set_mode(screen_size)

pygame.display.set_caption("Live game")
events = pygame.event.get()
clock = pygame.time.Clock()
current_game_state = GAME_STATE_PAUSE


def dead_or_alive():
    if random.random() < CELL_BIRTH_POSSIBILITY:
        return CELL_STATE_ALIVE
    else:
        return CELL_STATE_DEAD


first_state = [[0 for i1 in range(NUM_H_ELEMENTS)] for i in range(NUM_V_ELEMENTS)]
second_state = [[0 for i2 in range(NUM_H_ELEMENTS)] for i3 in range(NUM_V_ELEMENTS)]


# Метод для рисования

def toggle_cells():
    """
    Изменяет состояние клетки с живой на мертвую и обратно
    :return:
    """
    for iter in pygame.event.get():
        if iter.type == pygame.MOUSEBUTTONDOWN:
            if iter.button == pygame.BUTTON_LEFT:
                x, y = pygame.mouse.get_pos()

                # Элемент по горизонтали
                xi = int(x / QUB_LEN)
                yi = int(y / QUB_HIGH)

                if xi < 0 or xi > NUM_H_ELEMENTS - 1:
                    continue

                if yi < 0 or yi > NUM_V_ELEMENTS - 1:
                    continue

                if first_state[yi][xi] == CELL_STATE_DEAD:
                    first_state[yi][xi] = CELL_STATE_ALIVE
                else:
                    first_state[yi][xi] = CELL_STATE_DEAD


# Методы кнопочек
stop_button = Button(
    screen, 10, QUB_HIGH * NUM_V_ELEMENTS + 18, 40, 33, text='Stop',
    fontSize=15, margin=50,
    inactiveColour=RED,
    pressedColour=RED, radius=10,
    onClick=lambda: clear_game()
)
create_life_button = Button(
    screen, 55, QUB_HIGH * NUM_V_ELEMENTS + 18, 40, 33, text='Birth',
    fontSize=15, margin=50,
    inactiveColour=(66, 173, 66),
    pressedColour=RED, radius=10,
    onClick=lambda: create_life()
)
pause_button = Button(
    screen, 100, QUB_HIGH * NUM_V_ELEMENTS + 18, 45, 33, text='Pause',
    fontSize=15, margin=50,
    inactiveColour=(88, 153, 232),
    pressedColour=RED, radius=10,
    onClick=lambda: pause()
)
play_button = Button(
    screen, 150, QUB_HIGH * NUM_V_ELEMENTS + 18, 40, 33, text='Play',
    fontSize=15, margin=50,
    inactiveColour=(222, 118, 218),
    pressedColour=RED, radius=10,
    onClick=lambda: play()
)
save_button = Button(
    screen, QUB_HIGH * NUM_H_ELEMENTS - 50, QUB_HIGH * NUM_V_ELEMENTS + 18, 40, 33, text='Save',
    fontSize=15, margin=50,
    inactiveColour=(133, 133, 133),
    pressedColour=RED, radius=10,
    onClick=lambda: save_game_state()
)
load_button = Button(
    screen, QUB_HIGH * NUM_H_ELEMENTS - 100, QUB_HIGH * NUM_V_ELEMENTS + 18, 40, 33, text='Load',
    fontSize=15, margin=50,
    inactiveColour=(179, 179, 179),
    pressedColour=RED, radius=10,
    onClick=lambda: load_game_state()
)


def button_draw():
    stop_button.listen(events)
    create_life_button.listen(events)
    pause_button.listen(events)
    play_button.listen(events)
    save_button.listen(events)
    load_button.listen(events)
    stop_button.draw()
    create_life_button.draw()
    pause_button.draw()
    play_button.draw()
    save_button.draw()
    load_button.draw()

def clear_game():
    for x, y, qub in x_y_counter():
        # TODO: не понятно зачем условие
        if qub == CELL_STATE_ALIVE or qub == CELL_STATE_DEAD:
            first_state[y][x] = CELL_STATE_DEAD

def create_life():
    for x, y, qub in x_y_counter():
        # TODO: константа?
        if qub == 0:
            first_state[y][x] = dead_or_alive()

def play():
    global current_game_state
    current_game_state = GAME_STATE_PLAY
    
def pause():
    global current_game_state
    current_game_state = GAME_STATE_PAUSE


#Методы сохранения игры


def save_game_state():
    file = open(r'game_state', 'wb')
    pickle.dump(second_state, file)
    file.close()

def load_game_state():
    global first_state
    obj = pickle.load(open(r'game_state', 'rb'))
    first_state = copy.deepcopy(obj)


# Методы клеточек


def torr_universe(x, y):
    if x > NUM_H_ELEMENTS - 1:
        x -= NUM_H_ELEMENTS
        return x
    elif x < 0:
        x += NUM_H_ELEMENTS - 1
        return x

    if y > NUM_V_ELEMENTS - 1:
        y -= NUM_V_ELEMENTS
        return y
    elif y < 0:
        y += NUM_V_ELEMENTS - 1
        return y

def x_y_counter():
    """
    Получает всех координаты клеточек и их значний
    :return: x, y, value для каждой клеточки
    """
    for y, qubs in enumerate(first_state):
        for x, qub in enumerate(qubs):
            yield x, y, qub

def draw_cells():
    """
    Отрисовывает поле в текущем состоянии
    """
    for yi, qubs in enumerate(first_state):
        y = yi * QUB_HIGH
        for xi, qub in enumerate(qubs):
            x = xi * QUB_LEN
            if qub == 1:
                pygame.draw.rect(screen, GREEN, (x, y, QUB_LEN, QUB_HIGH), 0)
            if qub == 0:
                pygame.draw.rect(screen, WHITE, (x, y, QUB_LEN, QUB_HIGH), 0)

    pygame.display.flip()

def count_around_cells_alive(x, y):
    """
    Рассчитывает количетво живых соседних клеток вокруг клетки
    :param x: номер по горизонтали
    :param y: номер по вертикали
    :return: количество соседей
    """
    neigh_list = [(x - 1, y + 1), (x, y + 1), (x + 1, y + 1), (x - 1, y),
                  (x + 1, y), (x - 1, y - 1), (x, y - 1), (x + 1, y - 1)]
    count_alive = 0
    for x1, y1 in neigh_list:
        if x1 < 0 or x1 > NUM_H_ELEMENTS - 1:
            if TORR_UNIVERSE:
                x1 = torr_universe(x1, y1)
            else:
                continue
        if y1 < 0 or y1 > NUM_V_ELEMENTS - 1:
            if TORR_UNIVERSE:
                y1 = torr_universe(x1, y1)
            else:
                continue
        count_alive += first_state[y1][x1]
    return count_alive

def cells_update():
    """
    Функция для каждого цикла игры
    расчитывает изменения клеток
    :return
    """
    # TODO: объяснить в чем была ошибка и как исправлено на примере летающего планера
    for x, y, qub in x_y_counter():
        if count_around_cells_alive(x, y) == 3 and qub == CELL_STATE_DEAD:
            second_state[y][x] = CELL_STATE_ALIVE
        elif count_around_cells_alive(x, y) < 2 or count_around_cells_alive(x, y) > 3:
            second_state[y][x] = CELL_STATE_DEAD
        else:
            second_state[y][x] = qub


def state_update():
    global first_state
    first_state = copy.deepcopy(second_state)

def screen_update():
    running = True
    screen.fill(WHITE)
    while running:
        for event in events:
            if event.type == pygame.QUIT:
                running = False

        # Отрисовка кнопок
        button_draw()

        # Обработка мышки для включения/отключения клеток
        toggle_cells()
        if current_game_state == GAME_STATE_PLAY:
            cells_update()
            # Обновление состояния клеток
            state_update()

        # Перерисовка клеток
        draw_cells()

        # скорость обновления в секунду
        # ожидение внутри цикла
        clock.tick(CHANGE_SLIDE_SPEED)


screen_update()
pygame.quit()
