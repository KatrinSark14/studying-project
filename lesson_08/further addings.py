class BaseCell():
    def dead_or_alive(self):
        if random.random() < self.possibility:
            return ALIVE
        else:
            return DEAD


class Virus(BaseCell):
    possibility = 0.04
    color = RED


class Liver(BaseCell):
    possibility = 0.21
    color = GREEN