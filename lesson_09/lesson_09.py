import requests
from bs4 import BeautifulSoup
from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from aiogram.contrib.middlewares.logging import LoggingMiddleware


# Токен бота
bot = Bot(token='1615473582:AAF0HiSHEqogm-9cKMFNMnz_w0jAWKePUkA')
bot_starter = Dispatcher(bot)

# Подключаем логгирование
bot_starter.middleware.setup(LoggingMiddleware())


# Выгрузка гороскопа для кнопочек
def zodiac_horoscope(url, client_sign):
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'lxml')
    text_peace = 0
    date_peace = 0
    sign_peace = 0
    signs = soup.find_all('span', class_=f'icon-xl i_{client_sign}')
    for sign in signs:
        sign_peace = sign.text
    predictions = soup.find_all('li', class_='multicol_item')
    for pre in predictions:
        peace = pre.find('p')
        if peace is not None:
            text_peace = pre.find('p').text
            break
    day_today = soup.find_all('li', class_='multicol_item')
    for day in day_today:
        peace1 = day.find('h2')
        if peace1 is not None:
            date_peace = peace1.text
            break
    result = f'💫Знак {sign_peace}:\n✨{text_peace}\n🌟{date_peace}'
    return result


# Кнопочки
aries = InlineKeyboardButton('♈ Овен', callback_data='aries')
taurus = InlineKeyboardButton('♉ Телец', callback_data='taurus')
gemini = InlineKeyboardButton('♊ Близнецы', callback_data='gemini')
cancer = InlineKeyboardButton('♋ Рак', callback_data='cancer')
leo = InlineKeyboardButton('♌ Лев', callback_data='leo')
virgo = InlineKeyboardButton('♍ Дева', callback_data='virgo')
libra = InlineKeyboardButton('♎ Весы',callback_data='libra')
scorpio = InlineKeyboardButton('♏ Скорпион', callback_data='scorpio')
sagittarius = InlineKeyboardButton('♐ Стрелец', callback_data='sagittarius')
capricorn = InlineKeyboardButton('♑ Козерог', callback_data='capricorn')
aquarius = InlineKeyboardButton('♒ Водолей', callback_data='aquarius')
pisces = InlineKeyboardButton('♓ Рыбы', callback_data='pisces')

inline_button = InlineKeyboardMarkup().add(aries, taurus, gemini, cancer, leo, virgo, libra, scorpio
                                           , sagittarius, capricorn, aquarius, pisces)


@bot_starter.callback_query_handler()
async def process_callback_scorpio(callback_query: types.CallbackQuery):
    await bot.answer_callback_query(callback_query.id)
    await bot.send_message(callback_query['from']['id'],
                           zodiac_horoscope('https://goroskop.i.ua/%s/c/' % callback_query.data, callback_query.data))


# Чатбот
@bot_starter.message_handler(commands=['start'])
async def process_start_command(message: types.Message):
    await message.reply('🔮Выбери свой знак зодиака и получи гороскоп на сегодня🔮 ',  reply_markup=inline_button)


# Запускает программу
if __name__ == '__main__':
    executor.start_polling(bot_starter)