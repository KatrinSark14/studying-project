import requests
import logging

# These two lines enable debugging at httplib level (requests->urllib3->http.client)
# You will see the REQUEST, including HEADERS and DATA, and RESPONSE with HEADERS but without DATA.
# The only thing missing will be the response.body which is not logged.

import http.client as http_client
http_client.HTTPConnection.debuglevel = 1

# You must initialize logging, otherwise you'll not see debug output.
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True

headers = {
    #'authority': 'login.vk.com',
    'method': 'POST',
    'scheme': 'https',
    #'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    #'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
    'cache-control': 'max-age=0',
    'content-type': 'application/x-www-form-urlencoded',
    #'cookie': 'remixlang=0; remixstid=1099361441_4zA6BwZKMDbcVxLzM6bCPQLzsW2ZI177R6iqAp1ljmP; remixlhk=500952ff326b20a148; remixscreen_width=1280; remixscreen_height=720; remixscreen_dpr=1.5; remixscreen_depth=24; remixdt=0; tmr_lvid=fcff01b104df317b5bbaf74651dfbe18; tmr_lvidTS=1608312134349; remixbdr=0; remixflash=0.0.0; remixscreen_orient=1; remixscreen_winzoom=1; remixgp=ce95ec869a5d516f10f5158474ff4fc7; tmr_reqNum=3',
    'cookie': 'remixlang=0; remixstid=1099361441_4zA6BwZKMDbcVxLzM6bCPQLzsW2ZI177R6iqAp1ljmP; remixscreen_width=1280; remixscreen_height=720; remixscreen_dpr=1.5; remixscreen_depth=24; remixdt=0; tmr_lvid=fcff01b104df317b5bbaf74651dfbe18; tmr_lvidTS=1608312134349; remixbdr=0; remixflash=0.0.0; remixscreen_orient=1; remixgp=ce95ec869a5d516f10f5158474ff4fc7; remixttpid=03959f8bdf9e57a43fa4810592917efed447ea548c; remixusid=NjNkY2RhODQzNmQ0MGM0ODJjODIzYThi; remixseenads=0; remixscreen_winzoom=1.24; tmr_reqNum=8; tmr_detect=0%7C1611250146768; remixsid=fbf1906591cde9d453c6bcbbd64d2f4c58794113ba51afd9601f4eb77b4dc; remixtmr_login=1',
    'origin': 'https://vk.com',
    'referer': 'https://vk.com/',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36'
}

data_dict = {
    'act': 'login',
    'role': 'al_frame',
    'expire': '',
    'to': 'aW5kZXgucGhw',
    'recaptcha': '',
    'captcha_sid': '',
    'captcha_key': '',
    '_origin': 'https://vk.com',
    'ip_h': 'b7dd2364bc060cdfdd',
    'lg_h': 'ec24d2a7b6b87fb051',
    'ul': '',
    'email': 'katrin_sark%40mail.ru',
    'pass': 'ApresMoi2014',

}


s = requests.Session()
s.post('https://login.vk.com/?act=login', headers=headers,  data=data_dict)
print(s.cookies)

data = requests.post('https://vk.com/al_search.php',  data='act=search_request&al=1&c%5Bphoto%5D=1&c%5Bq%5D=boris&c%5Bsection%5D=people&change=1&search_loc=friends%3Fact%3Dfind')

f = open("a.html", "w")
f.write(data.text)
f.close()

