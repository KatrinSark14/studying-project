# Задача 1.
# Переделать задачу 6.5 так чтобы использовать кортежи вместо словаря.
# Переделать задачу 6.5 так чтобы использовать класс для точки.
# - Добавить третью координату в классе
# - добавить метод для класса, для рассчет расстояния до другой точки

import random
from math import *

n = random.randint(1, 10)


class Point:
    def __init__(self):
        self.x = random.randint(1, 10)
        self.y = random.randint(1, 10)
        self.z = random.randint(1, 10)

    def __str__(self):
        return "x: %s, y: %s, z: %s" % (self.x, self.y, self.z)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.x == other.x and self.y == other.y and self.z == other.z
        return False

    def point_len(self, p2):
        lenth = sqrt((p2.x - self.x) ** 2 + (p2.y - self.y) ** 2 + (p2.z - self.z) ** 2)
        return lenth


point_list = []
for i in range(20):
    new_point = Point()
    point_list.append(new_point)

result_list = []
for point1 in point_list:
    for point2 in point_list:
        if point1 != point2:
            result_list.append(point1.point_len(point2))
        if point2 == point1:
            break
print(min(result_list))
print(result_list)

# Задача 4.
# # Сделать пиксельный редактор с помощью PyGame.
# # - Сетка 100х100
# # - выбор цвета (внизу палитра из 200 цветов)
# # - при клике на цвет палитры выбирается цвет
# # - пли клике на сетку он окрашивается выбранным цветом

import pygame as pg
import pygame_widgets as pw
import sys

pg.init()

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
GREY = (145, 145, 145, 255)
screen = pg.display.set_mode((500, 600))
screen.fill(BLACK)


class GameSet:
    button = pw.Button(
        screen, 460, 510, 30, 30, text='Exit',
        fontSize=15, margin=50,
        inactiveColour=RED,
        pressedColour=RED, radius=10,
        onClick=lambda: sys.exit()
    )

    def __init__(self):
        pg.display.update()


class Palette:
    x = 0
    y = 510
    for qub_vert in range(8):
        for qub_hor in range(25):
            palette = pg.draw.rect(screen, [random.randint(0, 255) for _ in range(3)], (x, y, 10, 10), )
            x += 10
        x -= 250
        y += 10

    x_y = [0, 510]
    for checks_vert in range(8):
        for checks_hor in range(25):
            scale = pg.draw.rect(screen, BLACK, (x_y[0], x_y[1], 10, 10), 1)
            x_y[0] += 10
        x_y[0] -= 250
        x_y[1] += 10


class Scale():
    my_color = (79, 79, 79)
    scal_s = []
    x = 0
    y = 0
    for qub_vert in range(50):
        for qub_hor in range(50):
            scal = pg.draw.rect(screen, my_color, (x, y, 10, 10), )
            x += 10
            scal_s.append(scal)
        x -= 500
        y += 10

    x_y = [0, 0]
    for checks_vert in range(50):
        for checks_hor in range(50):
            scale = pg.draw.rect(screen, BLACK, (x_y[0], x_y[1], 10, 10), 1)
            x_y[0] += 10
        x_y[0] -= 500
        x_y[1] += 10

    m_color = [random.randint(0, 255) for _ in range(3)]


res = []
while 1:
    GameSet()
    for i in pg.event.get():
        if i.type == pg.QUIT:
            sys.exit()
        GameSet.button.listen(i)
        GameSet.button.draw()
        if i.type == pg.MOUSEBUTTONUP:
            if i.button == 3:
                color = screen.get_at(pg.mouse.get_pos())
                res.clear()
                res.append(color)
            if i.button == 1:
                for rect in Scale.scal_s:
                    pos = pg.mouse.get_pos()
                    if rect.collidepoint(pos):
                        screen.get_at(pg.mouse.get_pos())
                        pg.draw.rect(screen, res[0], (rect[0], rect[1], 10, 10))

