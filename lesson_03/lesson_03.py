# Сделать comprehesion list для B_list 
import random
import sys

N = int(input('Введите число от 1 до 20: '))

if N > 20 or N < 1:
    sys.exit('Ошибочка вышла')

B_list = [str(random.randint(0, 100)) for c in range(N)]
#for key in B_list:
#    print(key)
    
# Сделать все b_list без цикла + join 
# пока думала, придумала как без join и цикла, теперь попробую придумать с join:)
print('\n'.join(B_list))
